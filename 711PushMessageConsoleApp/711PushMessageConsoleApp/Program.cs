﻿using System;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;

namespace SevenElevenPushMessageConsoleApp
{
    class Program
    {
        private static string _firstFuelLockQuery = @"select count(*) from (select A.Id from dbo.Consumer A
                                                        inner join dbo.Consumer_Map B on A.EmailAddress = B.VMobUsername
                                                        inner join dbo.FuelLockEvents C on B.TouchAcountID = C.TouchConsumerId
                                                        where cast(C.CreatedDateTime as date) > dateadd(d, 2 * -1, getutcdate())
                                                        group by A.Id) as groupById";

        private static string _noFuelLockCreationInLast14Days = @"select count(*) from (select A.Id
                                                                    from dbo.Consumer A
                                                                      inner join dbo.Consumer_Map B on A.EmailAddress = B.VMobUsername
                                                                      inner join dbo.FuelLockEvents C on B.TouchAcountID = C.TouchConsumerId
                                                                    group by A.Id
                                                                    having
                                                                      datediff(
                                                                          day,
                                                                          max(cast(C.CreatedDateTime as date)),
                                                                          cast(current_timestamp as date)
                                                                      ) >= 14) as groupById";

        private static string _notRegisteredXDaysAfterInstallation = @"select count(*) from (select
                                                                          Id
                                                                        from dbo.Consumer
                                                                        where
                                                                          datediff(day, cast(CreationDate as date), cast(current_timestamp as date)) = {0}
                                                                          and EmailAddress is null) as notRegistered";

        private static string _notRegisteredAndNotOpenAppInLastXDays = @"select count(*) from (select
                                                                              Id
                                                                            from dbo.Consumer
                                                                            where
                                                                              POWER(convert(bigint, 2), {0} % 63) & Taggroup4Flags > 0
                                                                              and EmailAddress is null) as notUsed";

        static int _numberOfConsumersProcessEachTime = 100;
        static int _workflowId = 2;

        static void Main(string[] args)
        {
            for (int i = 0; i < args.Length; i++)
            {
                if (args[i] == "-workflowId")
                {
                    int.TryParse(args[i + 1], out _workflowId);
                }
                else if (args[i] == "-numberOfConsumers")
                {
                    int.TryParse(args[i + 1], out _numberOfConsumersProcessEachTime);
                }
            }

            CallLogicApp(_workflowId);
        }

        static void CallLogicAppWithWorkflowIdAndOffset(int workflowId, int offset)
        {
            var url = GetLogicAppByWorkflowId(workflowId);
                
            HttpWebRequest myReq = (HttpWebRequest)WebRequest.Create(url);
            myReq.ContentType = "application/json";
            myReq.Method = "POST";

            var body = GetLogicAppBody(offset, workflowId);
            byte[] byteArray = Encoding.UTF8.GetBytes(body);
            myReq.ContentLength = byteArray.Length;

            using (Stream dataStream = myReq.GetRequestStream())
            {
                dataStream.Write(byteArray, 0, byteArray.Length);
                Thread.Sleep(300);
            }
        }

        private static string GetLogicAppBody(int offset, int workflowId)
        {
            switch (workflowId)
            {
                case 1:
                case 2:
                    return "{\"offset\":" + offset + ", \"numberOfConsumers\":" + _numberOfConsumersProcessEachTime + "}";

                case 3:
                case 4:
                case 5:
                    return "{\"offset\":" + offset + ", \"numberOfConsumers\":" + _numberOfConsumersProcessEachTime + ", \"workflowId\":" + workflowId + ", \"numberOfDays\":" + GetNumberOfDaysForNotRegisteredAfterXDaysByWorkflowId(workflowId) + "}";

                case 6:
                case 7:
                case 8:
                    return "{\"offset\":" + offset + ", \"numberOfConsumers\":" + _numberOfConsumersProcessEachTime + ", \"workflowId\":" + workflowId + ", \"flagPosition\":" + GetFlagPositionByWorkflowId(workflowId) + ", \"numberOfDays\":" + GetNumberOfDaysForNotUsedAfterXDaysByWorkflowId(workflowId) + "}";
            }

            throw new Exception("workflowId " + workflowId + " is not supported for this method.");
        }

        private static object GetNumberOfDaysForNotRegisteredAfterXDaysByWorkflowId(int workflowId)
        {
            switch (workflowId)
            {
                case 3:
                    return 7;
                case 4:
                    return 30;
                case 5:
                    return 60;
            }

            throw new Exception("workflowId " + workflowId + " is not supported for this method.");
        }

        private static object GetNumberOfDaysForNotUsedAfterXDaysByWorkflowId(int workflowId)
        {
            switch (workflowId)
            {
                case 6:
                    return 30;
                case 7:
                    return 60;
                case 8:
                    return 90;
            }

            throw new Exception("workflowId " + workflowId + " is not supported for this method.");
        }

        private static string GetLogicAppByWorkflowId(int workflowId)
        {
            switch (workflowId)
            {
                case 1: //FirstFuelLock
                    return "https://prod-02.australiaeast.logic.azure.com:443/workflows/f3eb6c20bfd54f22932dbc50333db935/triggers/manual/run?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=CTB3amYkXcVC9GvJB3MkKeV8VcKqPxw2Mo0e_oBPE3Y";

                case 2: //NoFuelLockCreatedInLast14Days
                    return "https://prod-04.australiaeast.logic.azure.com:443/workflows/0783478077044db6a49d72fd1261f4cd/triggers/manual/run?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=5_Lfhc6YL3TL58c-EzAuJS0TCMiGT1c9Afh6WxPSsdU";

                case 3: //NotEmailRegistered7DaysAfterInstallation
                case 4: //NotEmailRegistered30DaysAfterInstallation
                case 5: //NotEmailRegistered60DaysAfterInstallation
                    return "https://prod-06.australiaeast.logic.azure.com:443/workflows/5b78b54ac9d04a5dbf12e65592053b22/triggers/manual/run?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=SoFjy0fLhPSsHMAXTE4ND-nABmFCvxnlZKuf7nW0AIA";

                case 6: //NotRegisteredAndNotOpenAppForLast30Days
                case 7: //NotRegisteredAndNotOpenAppForLast60Days
                case 8: //NotRegisteredAndNotOpenAppForLast90Days
                    return "https://prod-00.australiaeast.logic.azure.com:443/workflows/8ab5ef3ae56f4fe6aec4cc4d96fdb0b4/triggers/manual/run?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=O1UTnwfHiVXehPmtpRwDlLLadLP3Pq9SHxnlA6V3quc";

                default:
                    return "https://prod-02.australiaeast.logic.azure.com:443/workflows/f3eb6c20bfd54f22932dbc50333db935/triggers/manual/run?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=CTB3amYkXcVC9GvJB3MkKeV8VcKqPxw2Mo0e_oBPE3Y";
            }
        }

        static void CallLogicApp(int workflowId)
        {
            var queryString = GetConsumerIdCountQueryString(workflowId);
            var consumerIdCount = GetConsumerIdCount(queryString);

            int count = consumerIdCount / _numberOfConsumersProcessEachTime + (consumerIdCount % _numberOfConsumersProcessEachTime == 0 ? 0 : 1);

            for (int i = 0; i < count; i++)
            {
                CallLogicAppWithWorkflowIdAndOffset(workflowId, i * _numberOfConsumersProcessEachTime);

                if (i < count - 1)
                {
                    Thread.Sleep(300000); //wait for 5mins
                }
            }
        }

        static string GetConsumerIdCountQueryString(int workflowId)
        {
            switch (workflowId)
            {
                case 1:
                    return _firstFuelLockQuery;

                case 2:
                    return _noFuelLockCreationInLast14Days;

                case 3:
                case 4:
                case 5:
                    return string.Format(_notRegisteredXDaysAfterInstallation, GetNumberOfDaysForNotRegisteredAfterXDaysByWorkflowId(workflowId));

                case 6:
                case 7:
                case 8:
                    return string.Format(_notRegisteredAndNotOpenAppInLastXDays, GetFlagPositionByWorkflowId(workflowId));

                default:
                    return _firstFuelLockQuery;
            }
        }

        private static int GetFlagPositionByWorkflowId(int workflowId)
        {
            switch (workflowId)
            {
                case 6:
                    return 242;
                case 7:
                    return 79;
                case 8:
                    return 80;
            }

            throw new Exception("workflowId " + workflowId + " is not supported for this method.");
        }

        static int GetConsumerIdCount(string query)
        {
            var connetionString = "Data Source=bjpnk8kvkg.database.windows.net,1433;Initial Catalog=Vmob_tfn370_DWHS;User ID=vmob;Password=1yKm8kK3wo";
            var cnn = new SqlConnection(connetionString);
            try
            {
                cnn.Open();
                SqlCommand command = new SqlCommand(query);
                command.Connection = cnn;
                int count = int.Parse(command.ExecuteScalar().ToString());
                cnn.Close();
                return count;
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to execute query with error: " + ex.Message);
            }
        }
    }
}
